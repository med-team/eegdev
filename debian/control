Source: eegdev
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Nicolas Bourdaud <nicolas.bourdaud@gmail.com>,
           Étienne Mollier <emollier@debian.org>
Section: libs
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13),
               libdebhelper-perl,
               pkgconf,
               gnulib,
               bison,
               flex,
               libltdl-dev,
               libusb-1.0-0-dev,
               libexpat1-dev,
               libxdffileio-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/eegdev
Vcs-Git: https://salsa.debian.org/med-team/eegdev.git
Homepage: https://sourceforge.net/projects/eegdev/
Rules-Requires-Root: no

Package: libeegdev0t64
Provides: ${t64:Provides}
Replaces: libeegdev0
Breaks: libeegdev0 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: eegdev-plugins-free
Pre-Depends: ${misc:Pre-Depends}
Description: Biosignal acquisition device library
 eegdev is a library that provides a unified interface for accessing various
 EEG (and other biosignals) acquisition systems. This interface has been
 designed to be both flexible and efficient. The device specific part is
 implemented by the mean of plugins which makes adding new device backend
 fairly easy even if the library does not support them yet officially.
 .
 The core library not only provides to users a unified and consistent
 interfaces to the acquisition device but it also provides many
 functionalities to the device backends (plugins) ranging from configuration
 to data casting and scaling making writing new device backend an easy task.
 .
 This library is particularly useful to handle the acquisition part of a
 Brain Computer Interface (BCI) or any realtime multi-electrode acquisition
 in neurophysiological research.
 .
 This package contains the core library

Package: eegdev-plugins-free
Architecture: any
Multi-Arch: same
Depends: libeegdev0t64 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Biosignal acquisition device library (free plugins)
 eegdev is a library that provides a unified interface for accessing various
 EEG (and other biosignals) acquisition systems. This interface has been
 designed to be both flexible and efficient. The device specific part is
 implemented by the mean of plugins which makes adding new device backend
 fairly easy even if the library does not support them yet officially.
 .
 The core library not only provides to users a unified and consistent
 interfaces to the acquisition device but it also provides many
 functionalities to the device backends (plugins) ranging from configuration
 to data casting and scaling making writing new device backend an easy task.
 .
 This library is particularly useful to handle the acquisition part of a
 Brain Computer Interface (BCI) or any realtime multi-electrode acquisition
 in neurophysiological research.
 .
 This package contains the devices plugins that depends only on free
 components.

Package: libeegdev-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libeegdev0t64 (= ${binary:Version}),
         ${misc:Depends}
Description: Biosignal acquisition device library (Development files)
 eegdev is a library that provides a unified interface for accessing various
 EEG (and other biosignals) acquisition systems. This interface has been
 designed to be both flexible and efficient. The device specific part is
 implemented by the means of plugins which makes adding new device backend
 fairly easy even if the library does not support them yet officially.
 .
 The core library not only provides to users a unified and consistent
 interface to the acquisition device but it also provides many
 functionalities to the device backends (plugins) ranging from configuration
 to data casting and scaling making writing new device backend an easy task.
 .
 This library is particularly useful to handle the acquisition part of a
 Brain Computer Interface (BCI) or any realtime multi-electrode acquisition
 in neurophysiological research.
 .
 This package contains the files needed to compile and link programs which
 use eegdev. It provides also the headers needed to develop new device
 plugins. The manpages and examples are shipped in this package.
